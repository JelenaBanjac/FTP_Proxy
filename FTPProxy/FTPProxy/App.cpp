/**
 * Author: Jelena Banjac <jelena.b94@gmail.com>
 * Title: FTP proxy
 *
 * FTP documentation: https://tools.ietf.org/html/rfc959
 *
 */

#include <iostream>
#include <stdint.h>

#include "Proxy.h"
#include "logfile.h"

static FSMSystem sys(AUTOMAT_COUNT, MSGBOX_COUNT);

int mainProgram(int argc, char* argv[]) {
	Proxy proxy;

	const uint8 buffClassNo =  4; 
	uint32 buffsCount[buffClassNo] = { 50, 50, 5000, 10 }; 
 	uint32 buffsLength[buffClassNo] = { 128, 256, 512, 1024}; 

	LogFile lf("log.log", "./log.ini");
	LogAutomateNew::SetLogInterface(&lf);


	sys.InitKernel(buffClassNo, buffsCount, buffsLength, 3, Timer1s);
	sys.Add(&proxy, PROXY_AUTOMATE_TYPE_ID, 1, true);

	proxy.Start();

	printf("[*] Starting system...\n");
	sys.Start();

	

	return 0;
}