#include "fsm.h"
#include "MainTests.h"
#include "Constants.h"

CPPUNIT_TEST_SUITE_REGISTRATION( MainTests );
uint8 * msg;
uint16 msgcode;


void MainTests::setUp()
{
    pSys = new FSMSystem(4, 4);
	proxy = new Proxy();

    uint8 buffClassNo = 4;
    uint32 buffsCount[8] = { 100, 50, 50, 50 };
    uint32 buffsLength[8] = { 1025, 1025, 1025, 1025};

	pSys->InitKernel(buffClassNo, buffsCount, buffsLength, 1);

    lf = new LogFile("log.log", "log.ini");
    LogAutomateNew::SetLogInterface(lf);	

	pSys->Add(proxy, PROXY_AUTOMATE_TYPE_ID, 1, true);
}

void MainTests::tearDown() {

}

void MainTests::ProxyTestInit() {
	proxy->Start();

	msgcode = proxy->GetState();
	CPPUNIT_ASSERT_EQUAL((uint16)PROXY_IDLE, msgcode);
}

void MainTests::ProxyTestServerConnection() {
	proxy->waitForServerConnection();

	msgcode = proxy->GetState();
	CPPUNIT_ASSERT_EQUAL((uint16)PROXY_SERVER_CONNECTED, msgcode);
}

