#ifndef _Proxy_H_
#define _Proxy_H_

#include <fsm.h>
#include <fsmsystem.h>

#include "../kernel/stdMsgpc16pl16.h"
#include "NetFSM.h"

#include "Constants.h"

enum ProxyStates {	
	PROXY_IDLE, 
	PROXY_CLIENT_CONNECTED, 
	PROXY_READY_FOR_SERVER_RESPONSE,
	PROXY_READY_FOR_CLINET_REQUEST
};

enum ProxyEvents {
	PROXY_MSG_INITIAL_MSG,
	PROXY_MSG_SERVER_CONNECTION_SUCCESS,
	PROXY_MSG_CLIENT_CONNECTION_SUCCESS,
	PROXY_MSG_REQUEST_SENT,
	PROXY_MSG_RESPONSE_RECEIVED
};

class Proxy : public FiniteStateMachine {
	StandardMessage StandardMsgCoding;
	
	MessageInterface *GetMessageInterface(uint32 id);
	void	SetDefaultHeader(uint8 infoCoding);
	void	SetDefaultFSMData();
	void	NoFreeInstances();
	void	Reset();
	uint8	GetMbxId();
	uint8	GetAutomate();
	uint32	GetObject();
	void	ResetData();

public:
	void Initialize();
	Proxy();
	void waitForClientConnection();
	void waitForServerConnection();
	void processServerResponse();
	void processClientRequest();
	void Start();
	~Proxy();
	

protected:
	SOCKET serverSocket;
	SOCKET ftpClientSocket;
	SOCKET ftpServerSocket;

	sockaddr_in serverAddress;
	sockaddr_in ftpClientAddress;
	sockaddr_in ftpServerAddress;

	int ftpClientAddressSize;

	bool has2ServerResponses;
	bool waitForConnection;
	char frame[RECEIVE_BUFFER_LENGTH];
	int length;
};

#endif /* Proxy_H */